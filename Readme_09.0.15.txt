



       P L A T F O R M - S O F T W A R E - R E L E A S E - D O C U M E N T

                     Hirschmann Automation and Control GmbH
  
                        Platform Software Release 09.0.15

                                 2018/07/16

                               C O N T E N T S
                               ===============
 

    1.   Software Update
    1.1      Update over TFTP
    1.2      Update over ACA 21-USB
    1.3      Update over HTTP
    2.   Release Notes
    2.1      Web Browser
	2.2      HiView - the new way to interface with Hirschmann products
    2.3      Limitations of the Platform Release 09.0.15
    3.   Software Version Message
    4.   Contact
    5.   END USER LICENSE AGREEMENT
    6.   Copyright of integrated software



This document is delivered with the software as the file "Readme_09.0.15.txt".

 
.----------------------------------------------------------------------------.
| Before you start with the installation according to the 09.0.15 manual,    |
| please read this document carefully.                                       |
'----------------------------------------------------------------------------'


1. Software Update
==================
A software update replaces the installed operating software completely by the
new software release.

It is possible to upgrade to a new software version as well as to downgrade to
an older software version.

If a stored configuration exists, it will only be preserved by a software
upgrade. When performing a software downgrade, its contents may be lost
completely or partially.

If the downgrade to an older software version results in error messages and
repeated device reboots, then delete the configuration by entering the System
Monitor 1 and applying its menu "Erase main configuration file", please. If
you use an ACA, you have to delete the configuration file "*.cfg" on the ACA
manually. Alternatively, you can boot without the ACA.

.----------------------------------------------------------------------------.
|  Prior to a software update, disconnect all redundant connections.         |
'----------------------------------------------------------------------------'

+-------------------------  C A U T I O N -----------------------------------+
| When updating, please consider that an unsaved configuration               |
| will be lost and the default values will become active.                    |
+-------------------------  C A U T I O N -----------------------------------+

.----------------------------------------------------------------------------.
|  Please be patient while updating, the update process needs some           |
|  minutes to complete.                                                      |
'----------------------------------------------------------------------------'

+-------------------------  C A U T I O N  L2E and L2B ----------------------+
| On all L2E and L2B variants there is no backup image. Make sure that the   |
| update has ended successfully before retarting the switch.                 |
+----------------------------------------------------------------------------+

+-------------------------  C A U T I O N  Octopus L2P -------------------------+
| To actualize the firmware from version 07.0.03 or older to version 07.1.00    |
| or higher, do following steps:                                                |
| * Actualize the firmware to version 07.0.03.                                  |
| * Actualize the bootcode to version 05.0.00 or higher.                        |
|   To load the bootcode file over tftp into the device, use following CLI      |
|   command �copy <url> system:bootcode". For <url> use the path of the         |
|   tftp server in the following form: �<tftp://ip/filepath/fileName>",         |
|   for example �tftp://10.1.112.214/switch/octL2P_boot.img.".                  |
| * Afterwards actualize the firmware to version 07.1.00 or higher.             |
+-------------------------------------------------------------------------------+


After a succesful update, the switch can be restarted.

.----------------------------------------------------------------------------.
|  Check the redundancy configuration again before you reconnect all         |
|  redundant connections.                                                    |
'----------------------------------------------------------------------------'

1.1 Update over TFTP
====================
Before starting the update process, the new software has to be installed
on your tftp server and the tftp server application has to be configured.
Please refer to chapter 4 of your basic configuration user manual. 

First create the directory, e. g. "pmL2P/releasenumber" in the default directory
of your tftp server. Then copy the file, e. g. "pmL2P_09015.bin", to the new
directory. For more platform release images, repeat the above steps accordingly.

If you do not have a DHCP server, please configure the IP parameters 
via V.24 (line speed: 9600 bps, User: "admin", default password: "private").
Details about the IP configuration using the Command Line Interface can be found
in chapter 2 of your basic configuration user manual.

Enter the path (URL) to the update image file in the following format:
"tftp://<IP Address of TFTP Server>/pmL2P/releasenumber/pmL2P_09015.bin"
(E.g.: "tftp://192.168.1.2/pmL2P/releasenumber/pmL2P_09015.bin"). 

Now start the update.

IF the transfer aborts unsuccessfully with CRC or timeout error messages,
increase the TFTP server's timeout and retry settings and start the software
update once again.


1.2 Update over ACA 21-USB
==========================
The ACA 21-USB facilitates a local update of the platform software release.

Prior to the actual software update, load the software onto the ACA 21-USB with
help of your local computer.

.----------------------------------------------------------------------------.
|  For the device MS4128-5, use "powermice.bin" as the file name.            |
'----------------------------------------------------------------------------'

Then connect the ACA 21-USB to the switch and start the update with the help of
the System Monitor 1. Proceed as described below:

1. Coldstart the device
2. Press the key <1> when following message appears:
   "Press <1> to enter System Monitor 1"
3. After that the menu "System Monitor 1" is displayed
4. Press the key <2> "Update Operating system"
5. After that the following message is displayed:
   "Update Operating System from USB" -> "Enter filename: /usb0/"
6. Enter the name of your image file, e. g., pmL2P_09015.bin
   and confirm the input
7. After that the image will be transfered from the ACA 21-USB to the device's
   internal flash.
8. After a successfull update, press the key <4> "End (reset and reboot)"

Details on the System Monitor 1 are described in chapter 1 of the 
basic configuration user manual.


1.3 Update over HTTP
====================
For a HTTP update you need access from your computer to the update
software. Now open the web interface (see chapter 2.1).
In the dialog "Software", click on "http Update".
A second browser window will be opened. Here you select the update software
and select "Update" to transfer the software.

 
2. Release Notes
================
2.1. Web Browser
================
The Platform Software Release 09.0.15 is optimized for following web browsers:
 
Web browser:
- Mozilla 1.0 or later
- Netscape Navigator V 6.x or later
- Microsoft Internet Explorer V 6.x or later

For the correct function of the web interface, java and javascript 
have to be enabled in the web browser and a java plugin (JRE V7)
has to be installed (the plugin is available on the enclosed CD
or can be loaded from http://java.com/getjava/). 

In the Web browser, go to the address "http://<The management agent's IP address>".
Initial password: "private"

Alternatively, "ReadOnly" access is possible:
Initial password: "public"

2.2 HiView - the new way to interface with Hirschmann products
==============================================================
HiView is a stand-alone application. HiView thus allows you to use the
graphical user interface of Hirschmann Ethernet devices with management
independently of other applications, such as a browser.
HiView is convenient in that it allows you to store HiView on a portable
storage medium and start it on other computers in your data network.
Forget difficulties such as the incompatibility of browsers, Java versions
or Java plug-ins, installation with entries in the registry or the changing
cache content of browsers on different computers.
The enclosed CD contains the free of charge application HiView. 
HiView is also available at www.hivision.de or www.beldensolutions.com.
 
2.3 Limitations of the Platform Release 09.0.15
===============================================
(01) On twisted pair ports, automatic MDI/MDIX crossover is only available with
     autonegotiation enabled.
(02) In the default state (factory settings), all ring redundancy settings can be
     configured by software, the configuration via the DIP switch is deactivated.
(03) In the default state (factory settings), neither HIPER-Ring nor Redundant Ring
     Coupling are preconfigured.
(04) In the default state (factory settings), RSTP is enabled both globally
     and on all ports.
(05) Please note that on MS4128 and MACH, no more than 8000 different MAC addresses 
     may be used in the HIPER-Ring, otherwise the result can be a misbehaviour of  
     the redundancy.
(06) If the redundant ring coupling is switched off or the mode of the redundant
     ring coupling is changed between
     - outband (with control cable),
     - inband (without control cable) and
     - local (both coupling ports on the same device)
     while the cables at the respective ports remain plugged-in, the administrative
     port status of the control port and the coupling port will be set to "down",
     to prevent a permanent loop. These ports must be manually set to "up" again.
(07) The HIPER-Ring, the Redundant Ring Coupling, IEEE1588-2002 (PTPv1) and GMRP
     only work in VLAN 1.
(08) VLAN and HIPER-Ring: on the Redundancy Manager, the activation of VLAN Ingress
     Filtering is not allowed on ring ports or ring coupling ports.
(09) It is not allowed to plug-in or remove the ACA 21-USB while the switch is 
     booting.
(10) SNMPv3 traps are not supported
(11) IGMP Snooping: Unknown multicast traffic is per default flooded.
     With management this behaviour can be set to discard the traffic 
     or send to queryports.
(12) Broadcast/Multicast MAC Addresses: Some specific addresses can not be added
     to the MAC filter table, neither through management, GMRP nor via IGMP snooping.
     The following addresses are affected:
     - FF:FF:FF:FF:FF:FF (Broadcast)
     - 01:80:C2:00:00:00 - 01:80:C2:00:00:10 (IEEE Multicasts)
     - 01:80:C2:00:00:20 - 01:80:C2:00:00:2F (IEEE Multicasts)
     - 01:00:5E:00:00:00 - 01:00:5E:00:00:FF (IANA reserved Multicasts, 
                                              only on PowerMICE/MACH)
     - 01:00:5E:00:01:81 - 01:00:5E:00:01:84 (PTP Multicasts)
     - 01:00:5E:00:01:01 (SNTP Multicast)
(13) CLI inputs should have only following characters [0-9,a-z,A-Z]
(14) Disable global flow control if you want to use priorisation.
(15) Is "Unknown Multicast Filtering" activated on OpenRAIL/MICE products.
     no IP Multicasts in the range 224.0.0.1 - 224.0.0.255 (IANA-reserved) are switched.
     This can cause malfunction in routing protocols.
     Workaround: Define the corresponding MAC MC addresses as a static filter.
(16) On L2E/B variants loading the webinterface while updating the firmware is not possible.
(17) The simultaneous operation of HIPER-Ring and MRP is not possible.
(18) In the webdialog portconfiguration the current settings shows also the linkstatus.
(19) The gigabit TP-SFPs only support the auto-negotiation mode.
     The gigabit TP-SFPs can not be used in the fast ethernet slots.
	 The fast ethernet TP-SFPs can not be used in pure gigabit slots.
     The manual autocrossing configuration on TP-SFPs is not possible.
(20) The SNTP server will flood the answers when using more than 256 (128 on L2E, L2B)
     SNTP Clients.
(21) In the subring and in the MRP ring a maximum of 200 devices are possible.
(22) Using link aggregation in the subring or in the MRP ring is not possible.
(23) Cascading of subrings is not possible.
(24) 4 subring instances (subring ports) are possible on one device.
(25) RSTP must be deactivated on a subring port.
(26) The SVL mode is not possible when using subring with a MRP main ring.
(27) Beginning with version 04.2.06 the ring coupling sends the redundancy frames into 
     VLAN1 with highest priority.
(28) Multicast routing is not working on vlan routing interfaces if igmp snooping 
	 is enabled and forward-unknown is set to discard.
(29) If HTTPS is activated performance of the web interface might be reduced.
(30) The MIB2 counter ifInDiscards and ifOutDiscards are not supported and show always 0.
(31) With release 09.0.06 the Default value for SNMP password synchronization changed to �Disabled�.

Please read the associated documents for the limitations of the particular
releases. These are labeled for the correspondent devices as following:

- Readme_MACH4002XG.09.0.15.txt
- Readme_MACH4002.09.0.15.txt
- Readme_MICE.09.0.15.txt
- Readme_PowerMICE.09.0.15.txt
- Readme_Railswitch.09.0.15.txt
- Readme_Octopus.09.0.15.txt
- Readme_MAR1000.09.0.15.txt
- Readme_RSR.09.0.15.txt
- Readme_MACH100.09.0.15.txt
- Readme_MAR1040.09.0.15.txt
- Readme_MACH104.09.0.15.txt

 
3. Software Version Message
===========================
After a successful software update, the correct version should
be displayed upon entry of the CLI command "show running-config".
The version strings for this software release are:

MACH4002XG:
!System Version L2P-09.0.15
!System Version L3E-09.0.15
!System Version L3P-09.0.15

MACH4002:
!System Version L2P-09.0.15
!System Version L3E-09.0.15
!System Version L3P-09.0.15

MICE:
!System Version L2E-09.0.15
!System Version L2P-09.0.15

PowerMICE:
!System Version L2P-09.0.15
!System Version L3E-09.0.15
!System Version L3P-09.0.15

Railswitch:
!System Version L2E-09.0.15
!System Version L2P-09.0.15

OCTOPUS:
!System Version L2E-09.0.15
!System Version L2P-09.0.15

OCTOPUS OS 20/30:
!System Version L2P-09.0.15

MAR1000:
!System Version L2P-09.0.15

RSR:
!System Version L2P-09.0.15

MACH100:
!System Version L2P-09.0.15

MAR1040:
!System Version L2P-09.0.15
!System Version L3P-09.0.15

MACH104:
!System Version L2P-09.0.15
!System Version L3P-09.0.15


4. Contact
==========

           .----------------------------------------------------------------.
           |                                                                |
           |  If you have further questions, please contact your Hirschmann |
           |  contract partner.                                             |
           |                                                                |
           |  Current information and the latest news concerning our        |
           |  products can be accessed via our WWW Server on                |
           |  http://www.hirschmann-ac.com                                  |
           |                                                                |
           '----------------------------------------------------------------'

     .------------------------------------------------------------------------------.
     | Hirschmann Competence Center                                                 |
     |                                                                              |
     | In the long term, product excellence alone is not an absolute guarantee of a |
     | successful project implementation. Comprehensive service makes a difference  |
     | worldwide. In the current scenario of global competition, the Hirschmann     |
     | Competence Center stands head and shoulders above the                        |
     | competition with its comprehensive spectrum of innovative services:          |
     | - Consulting incorporates comprehensive technical advice, from system        |
     |   evaluation through network planning to project planning.                   |
     | - Training offers you an introduction to the technological fundamentals,     |
     |   product briefing and user training with certification.                     |
     | - Support ranges from commissioning through the standby service to           |
     |   maintenance concepts.                                                      |
     | With the Competence Center, you firmly rule out any compromise:              |
     | the client specific package leaves you free to choose the service            |
     | components that you will use.                                                |
     | Internet:                                                                    |
     | http://www.hicomcenter.com                                                   |
     `------------------------------------------------------------------------------'



5. END USER LICENSE AGREEMENT
=============================

These Hirschmann Software Conditions - Embedded Software constitute a legally-binding contract between Hirschmann Automation and Control GmbH, Stuttgarter Strasse 45-51, 72654 Neckartenzlingen (hereinafter referred to as "Hirschmann") and the Customer concerning the use of Hirschmann software products (machine-readable computer programs (including updates) as well as the associated media, printed materials and documentation in electronic format) in the form of embedded codes as part of a Hirschmann product, system or device (e.g. an industrial Ethernet switch)  (hereinafter referred to as the "embedded software"). 
These Hirschman Software Conditions - Embedded Software apply for all - including future - contracts, deliveries and other services which pertain to Hirschmann products, systems or devices where Hirschmann is the seller or contractor. By placing the order or awarding the contract, the customer declares itself in agreement with the following Hirschmann Software Conditions - Embedded Software. If the order is confirmed by the customer only on the basis of its own purchasing conditions, then Hirschmann hereby objects thereto. 
Our Conditions are deemed to have been accepted at the latest upon receipt or use of the Hirschmann products, systems or devices which are equipped with embedded software. 

1. Definitions
"Customer"
A Customer for the purposes of this contract is a natural person who or a legal entity which receives direct deliveries of the embedded software from Hirschmann.
"Third party"
A third party for the purposes of this contract is a natural person who or a legal entity which receives embedded software deliveries from the Customer and not from Hirschmann.

2. Subject of the Contract
Pursuant to this contract, the Customer receives without time limit the right of use of the embedded software delivered to it on the Hirschmann products, systems or devices provided therefor, as well as the use of necessary literature and documentation. The right of use is not exclusive and - insofar as nothing to the contrary arises from these Software Conditions - Embedded Software - is not transferable.
To the extent that it is necessary and not otherwise agreed upon between the contractual partners, installation of the embedded software will be performed by the Customer on its own responsibility and in accordance with the installation instructions. The selection of the Hirschmann products, systems or devices equipped with the embedded software and consultation regarding the applications intended by the Customer, and also instruction, training and other technical support of the Customer, are not a subject of this contract. They may be the subject of a separate contract. Without such agreement, only the Customer assumes the risk associated with the selection of the Hirschmann products, systems or devices equipped with the embedded software and their suitability for the applications intended. Hirschmann is liable in such an event only within the scope of sub-paragraph 9 of these Software Conditions.

3. Extent of Performance and Function
The extent of performance and function of the delivered programs is determined by the product descriptions valid at the time the contract is entered into.

4. Updates
Insofar as the embedded software is labelled or designated as an update, in order to use this, it is necessary to obtain a licence for a product which is defined by Hirschmann as suitable for the update (hereinafter referred to as a "suitable product"). A software product which is labelled as an update replaces and/or augments the starting product. The Customer may use the resulting updated product only in compliance with the provisions of these Software Conditions. 
Insofar as the embedded software is an update of a component of a software program package which the Customer has licensed as a unified product, the embedded software may be used only as part of that unified product package and may not be separated for use on more than one computer. 

5. License Fees
Fees for the right of use of the embedded software delivered are - insofar as nothing to the contrary is agreed upon - included as a once-off licence fee in the purchase price for the Hirschmann products, systems or devices equipped with the embedded software.

6. Right of Use
The embedded software may only be used on one item of the Hirschmann products, systems or devices provided therefor. Any additional use of the embedded software on further Hirschmann products, systems, devices or hardware requires a separate agreement with Hirschmann and is only permissible after payment of a corresponding fee for right of use. Copies may only be made for archive purposes, as replacements, or for fault diagnosis. All rights in the embedded software (including documentation), especially the right of copying, distribution and translation, remain the rights of Hirschmann. The Customer must ensure that the embedded software and documentation are not accessible to third parties without Hirschmann�s previous written permission. The copyright notice located on the original is to be affixed to all copies. 
The Customer may transfer the right of use of the embedded software to a third party (for instance, through a re-sale contract) only if the latter acknowledges these Hirschmann Software Conditions - Embedded Software and the Customer proves to Hirschmann without being requested to do so that it has handed over to the third party, deleted, destroyed or otherwise made unusable all of the tangible and intangible copies of the embedded software (including all components, media and printed materials and all updates). If the embedded software to be transferred is an update, the proven transfer, deletion, destruction or deactivation must also encompass all of the previous versions of the embedded software. 
With the transfer, all rights of use of the Customer are cancelled, including the rights of any copies, which are to be transferred to the third party. The Customer may not transfer the embedded software to a third party if there is a justifiable supposition that the third party might breach the conditions of the Hirschmann Software Conditions - Embedded Software, especially that it might create unauthorized copies. This is also valid with regard to employees of the Customer.
The Customer is not entitled to grant sub-licences concerning the embedded software to third parties.
In the event of a breach of the rights of use or upon a modification of the embedded software by the Customer, Hirschmann may withdraw the right of use from the Customer and - irrespective of other existing rights - demand the return or the destruction of the embedded software as well as that of any existing copies. The right of use of the embedded software may be terminated without notice by Hirschmann if a significant cause exists. A significant cause shall exist for Hirschmann especially if the Customer breaches the conditions of this contract and continues its actions in breach of contract, even though Hirschmann has warned it against such actions. The right of use is granted subject to the complete payment of the one-time license fee.

7. Proprietary Rights
With reservation of all rights of use granted under section 6, Hirschmann remains the proprietor of all rights, especially those of copyrighted exploitation rights, also those of copying, distribution and translation of the delivered programs, of the literature and documentation and similar items pertaining thereto, and of all complete or partial back-up copies made by the Customer within the scope of its use. If the program is delivered to the Customer only in machine code, then the Customer will not acquire access to the source code. The Customer is obligated to prevent unauthorized access by third parties to the embedded software, as well as to the documentation, by appropriate precautionary measures. Delivered original data carriers and back-up copies are to be maintained in a secure place to prevent unauthorized access by third parties. The Customer will instruct its employees in an appropriate manner concerning Hirschmann�s proprietary rights.

8. Obligation to Inspect and Report Defects
The Customer is obliged to examine the embedded software delivered and/or the Hirschmann product, system or device equipped with embedded software, including documentation, within 8 working days after delivery, especially with regard to data carriers and manuals, as well as to the operability of the basic program functions. Defects which are thereby ascertained or ascertainable must be reported to Hirschmann within without undue delay. The notification of defects must contain a best-efforts description in detail of the defects. Defects which are not ascertainable within the framework of the described and orderly examination must be reported within 8 working days after their discovery in compliance with the described defect requirements. Upon failure to fulfil the obligation to examine and inspect, the embedded software is considered as accepted with due regard for concerned defects.

9. Warranty 
The Customer is aware that, in accordance with the state-of-the-art technology, it is not possible to produce a computer program which is completely free of errors. Hirschmann warrants that the programs delivered as embedded software will fulfil the functional and performance features which are contained in the valid product descriptions at the time of entry into the contract, or which are separately agreed to. Excepted from this are defects which represent only non-essential deviations from the respective valid product description. 
Hirschmann assumes no responsibility that the embedded software will run without interruption or error, that all software errors can be eliminated by Hirschmann, and that the functions contained in the embedded software may be executed in all of the combinations selected by the Customer or that they correspond to its requirements. Hirschmann assumes the obligation to rectify software errors which impair use in compliance with the contract and which are not of an immaterial nature, specifically at Hirschmann's selection and, depending on the significance of the error, by the delivery of improved embedded software or through information regarding the elimination or through circumvention of the effects of the error. The precondition for the rectification of errors is that the effects of the errors be reproducible, that they be adequately described by the Customer, and that the error be reported to Hirschmann within the term stipulated in section 8. 
The warranty is excluded to the extent that the error is due to the Customer or a third party making modifications of any kind to the embedded software or data carriers or treating them in an improper manner. Hirschmann warrants that the data carrier is free from material or manufacturing defects. Hirschmann will replace any defective data carriers with data carriers which are free of defects. The Customer has the right to demand a reduction of the user�s fee upon an unsuccessful replacement shipment, or to rescind the contract without cost. In the event of rescission, the Customer will return to Hirschmann any data carrier with the embedded software and documentation pertaining thereto or will destroy any existing copies. For further claims, especially for compensation for damage which does not occur in the programs themselves, Hirschmann assumes liability pursuant to section 10.

10. Liability of the Licenser/Exclusion of Liability
Claims against Hirschmann as well as its employees or agents for compensation for damage, irrespective of the legal basis, are excluded, especially a claim for replacement of damage which does not occur to the embedded software itself. 
This does not apply to the extent that Hirschmann is liable under mandatory law, e.g. in the case of (i) personal injury or damage to privately-used property pursuant to the German Product Liability Act, (ii) due to loss of life, personal injury or damage to health which is due to a negligent or intentional breach of duty by Hirschmann or one of Hirschmann�s legal representatives or one of Hirschmann�s vicarious agents, (iii) insofar as the cause of damage or loss is due to intentional behaviour or gross negligence by Hirschmann or a legal representative or a vicarious agent of Hirschmann, (iv) if the Customer asserts rights due to a deficiency under a quality guarantee or the particular duration of a quality, (v) Hirschmann negligently breaches a fundamental contract obligation whose fulfilment is what makes the due performance of the contract possible at all and on whose compliance the Customer may regularly rely (cardinal obligation), or (vi) claims for recourse in the customer goods purchase supply chain (� 478 of the German Civil Code (BGB)) are concerned.  
Hirschmann is not liable for the replacement of data unless Hirschmann wilfully or through gross negligence caused the data�s destruction and the Customer has determined that that data can be reconstructed at reasonable cost from data material which is available in machine-readable form.
All claims directed against Hirschmann due to a quality defect or a defect in title become time-barred 12 months after the statutory warranty commencement, unless the German Product Liability Act or other legislation, particularly � 479.1 of the BGB (recourse claims in the consumer goods purchase supply chain) prescribe longer periods. The period of limitations for claims based on liability for damage and loss arising out of loss of life, personal injury or damage to health which is due to a negligent or intentional breach of duty by Hirschmann or a legal representative or vicarious agent of Hirschmann, and for other damage and loss which is due to an intentional or grossly negligent breach of duty by Hirschmann or a legal representative or vicarious agent of Hirschmann, shall be determined in accordance with the statutory provisions. 
The provisions set out in these Software Conditions - Embedded Software concerning the exclusion of liability (section 10) shall apply only if the Customer is a businessperson (� 14 of the BGB), a legal entity under public law or a public-law special fund. 

11. Return of the Program
If the program delivered based on this contract is partially or wholly exchanged within the framework of the warranty performance, then the Customer is obligated to verifiably destroy the original of the program or to return it to Hirschmann.

12. Termination of the Contract
The Customer is able to terminate the contract at any time in whole or in part.  The. In the event of the termination of this contract, the Customer is obliged to return to Hirschmann or verifiably destroy the original as well as all copies and partial copies of the programs concerned and also modified copies of the pertinent programs associated with other software materials. This applies correspondingly for program documentation and other delivered literature. Retention of an archive copy for back-up reasons requires a separate, written agreement. The license fee paid - insofar as nothing to the contrary is agreed - will not be refunded.

13. Written Form
Supplementary agreements and modifications must be in written form. This applies also to a waiver of this written-form requirement.

14. Choice of Law, Legal Venue
The application of German law is hereby agreed. For any disputes which should arise as a result of the contractual relationship when the Customer is a fully-qualified merchant, a legal entity under public law or a special fund under public law, then the court at the place of Hirschmann�s headquarters will have jurisdiction.

15. Partial Invalidity
If a condition of this contract should be or become invalid, the validity of the remaining conditions shall not be affected thereby. In such an event, the invalid condition is to be understood, interpreted or replaced so that the economic purpose pursued through it is achieved.

16. Export
The Customer will comply with the German and American export regulations when exporting the embedded software.


6. Copyright of integrated software
===================================

Bouncy Castle Crypto APIs (Java):
=================================
Copyright (c) 2000 The Legion Of The Bouncy Castle (http://www.bouncycastle.org) 

Permission is hereby granted, free of charge, to any person obtaining a copy of this
software and associated documentation files (the "Software"), to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to the following 
conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software. 

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

LVL7 SYSTEMS INC.
=================
Copyright (c) 1999-2006 LVL7 Systems, Inc. All Rights Reserved.

University of California
========================
Copyright (c) 1988, 1989, 1993
 The Regents of the University of California.  All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgment:
 This product includes software developed by the University of
 California, Berkeley and its contributors.
4. Neither the name of the University nor the names of its contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

University of Michigan
======================
Copyright (c) 1997, 1998, 1999

The Regents of the University of Michigan ("The Regents") and Merit Network,
Inc.  All rights reserved.
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1.  Redistributions of source code must retain the above 
    copyright notice, this list of conditions and the 
    following disclaimer.
2.  Redistributions in binary form must reproduce the above 
    copyright notice, this list of conditions and the 
    following disclaimer in the documentation and/or other 
    materials provided with the distribution.
3.  All advertising materials mentioning features or use of 
    this software must display the following acknowledgement:  
This product includes software developed by the University of Michigan, Merit
Network, Inc., and their contributors. 
4.  Neither the name of the University, Merit Network, nor the
    names of their contributors may be used to endorse or 
    promote products derived from this software without 
    specific prior written permission.
THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.  

OpenSSL 
=======

	 * Copyright (c) 1998-2008 The OpenSSL Project.  All rights reserved.
	 *
	 * Redistribution and use in source and binary forms, with or without
	 * modification, are permitted provided that the following conditions
	 * are met:
	 *
	 * 1. Redistributions of source code must retain the above copyright
	 *    notice, this list of conditions and the following disclaimer. 
	 *
	 * 2. Redistributions in binary form must reproduce the above copyright
	 *    notice, this list of conditions and the following disclaimer in
	 *    the documentation and/or other materials provided with the
	 *    distribution.
	 *
	 * 3. All advertising materials mentioning features or use of this
	 *    software must display the following acknowledgment:
	 *    "This product includes software developed by the OpenSSL Project
	 *    for use in the OpenSSL Toolkit. (http://www.openssl.org/)"
	 *
	 * 4. The names "OpenSSL Toolkit" and "OpenSSL Project" must not be used to
	 *    endorse or promote products derived from this software without
	 *    prior written permission. For written permission, please contact
	 *    openssl-core@openssl.org.
	 *
	 * 5. Products derived from this software may not be called "OpenSSL"
	 *    nor may "OpenSSL" appear in their names without prior written
	 *    permission of the OpenSSL Project.
	 *
	 * 6. Redistributions of any form whatsoever must retain the following
	 *    acknowledgment:
	 *    "This product includes software developed by the OpenSSL Project
	 *    for use in the OpenSSL Toolkit (http://www.openssl.org/)"
	 *
	 * THIS SOFTWARE IS PROVIDED BY THE OpenSSL PROJECT ``AS IS'' AND ANY
	 * EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
	 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE OpenSSL PROJECT OR
	 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
	 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
	 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
	 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
	 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
	 * OF THE POSSIBILITY OF SUCH DAMAGE.
	 * ====================================================================
	 *
	 * This product includes cryptographic software written by Eric Young
	 * (eay@cryptsoft.com).  This product includes software written by Tim
	 * Hudson (tjh@cryptsoft.com).
	 *
	 */

	 Original SSLeay License
	 -----------------------

	/* Copyright (C) 1995-1998 Eric Young (eay@cryptsoft.com)
	 * All rights reserved.
	 *
	 * This package is an SSL implementation written
	 * by Eric Young (eay@cryptsoft.com).
	 * The implementation was written so as to conform with Netscapes SSL.
	 * 
	 * This library is free for commercial and non-commercial use as long as
	 * the following conditions are aheared to.  The following conditions
	 * apply to all code found in this distribution, be it the RC4, RSA,
	 * lhash, DES, etc., code; not just the SSL code.  The SSL documentation
	 * included with this distribution is covered by the same copyright terms
	 * except that the holder is Tim Hudson (tjh@cryptsoft.com).
	 * 
	 * Copyright remains Eric Young's, and as such any Copyright notices in
	 * the code are not to be removed.
	 * If this package is used in a product, Eric Young should be given attribution
	 * as the author of the parts of the library used.
	 * This can be in the form of a textual message at program startup or
	 * in documentation (online or textual) provided with the package.
	 * 
	 * Redistribution and use in source and binary forms, with or without
	 * modification, are permitted provided that the following conditions
	 * are met:
	 * 1. Redistributions of source code must retain the copyright
	 *    notice, this list of conditions and the following disclaimer.
	 * 2. Redistributions in binary form must reproduce the above copyright
	 *    notice, this list of conditions and the following disclaimer in the
	 *    documentation and/or other materials provided with the distribution.
	 * 3. All advertising materials mentioning features or use of this software
	 *    must display the following acknowledgement:
	 *    "This product includes cryptographic software written by
	 *     Eric Young (eay@cryptsoft.com)"
	 *    The word 'cryptographic' can be left out if the rouines from the library
	 *    being used are not cryptographic related :-).
	 * 4. If you include any Windows specific code (or a derivative thereof) from 
	 *    the apps directory (application code) you must include an acknowledgement:
	 *    "This product includes software written by Tim Hudson (tjh@cryptsoft.com)"
	 * 
	 * THIS SOFTWARE IS PROVIDED BY ERIC YOUNG ``AS IS'' AND
	 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
	 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
	 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
	 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
	 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
	 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
	 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
	 * SUCH DAMAGE.
	 * 
	 * The licence and distribution terms for any publically available version or
	 * derivative of this code cannot be changed.  i.e. this code cannot simply be
	 * copied and put under another distribution licence
	 * [including the GNU Public Licence.]
	 */
