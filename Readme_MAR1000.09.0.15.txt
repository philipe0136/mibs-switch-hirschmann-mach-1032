
            S O F T W A R E - R E L E A S E - D O C U M E N T

                 Hirschmann Automation and Control GmbH
              
            	      MAR1000 Release 09.0.15
            		
            	 	     2018/07/16
             		
                           C O N T E N T S
                           ===============


    1.   Limitations of the release MAR1000 09.0.15
    2.   Hardware Considerations
    3.   History of Changes

   

This document is delivered with the software in the file 
"Readme_MAR1000.09.0.15.txt".

 

 -----------------------------------------------------------------------------
| For superior functions like software update,                                |
| supported web browser and limitations of the platform release 09.0.15       | 
| please read the document Readme_09.0.15.txt.                                |
 -----------------------------------------------------------------------------



1. Limitations of the release MAR1000 09.0.15
================================================
(01) Backpressure (flowcontrol in halfduplex) is not supported.
(02) In VLAN 0 transparent mode the configuration of VLAN 1 has an impact 
     to untagged and priority only tagged traffic.
(03) The egress limiter all is not supported on halfduplex ports.
(04) In the mode "Discard unknown multicasts" following standard multicast
     addresses were defined as static filters and so have the state known:
     - FF:FF:FF:FF:FF:FF (Broadcast)
     - 01:80:C2:00:00:00 (Spanning Tree)
     - 01:80:C2:00:00:20 (GMRP)
     - 01:80:C2:00:00:21 (GVRP)
     - 01:00:5E:00:01:01 (SNTP-Multicast)
     - 01:00:5E:00:01:81 - 01:00:5E:00:01:84 (PTP-Multicasts)
     - 01:1B:19:00:00:00 (PTPv2 L2 Multicast)
     - 01:0E:CF:00:00:00 (DCP)
     - 01:15:4E:00:00:01 - 01:15:4E:00:00:02 (MRP)
     - 01:00:5E:7F:10:0C (HiDiscoveryv2)
(05) Link aggregation in Hiper Ring or in the redundant ringcoupling
     is not supported.
(06) If the GOOSE protocol accordant to IEC61850-8-1 is used, than activate
     the "VLAN 0 transparent mode". 
     With this mode the IEEE802.1D/p priority information in the data packet,
     is still in the data packet after sending it out of the switch.
     Also for other protocols that use no IEEE802.1Q VLANs but need the 
     IEEE802.1D/p priority information you can use this mode.

     
2. Hardware Considerations
==========================
This platform software release 09.0.15 is released exclusively for the 
following hardware:

- Software marL2P.bin supports exclusively L2P hardware variants

- MAR1020, MAR1030  (Since SW 03.0.01)
- MAR1022, MAR1032  (Since SW 04.2.00)
- MAR1120, MAR1130  (Since SW 04.2.00)
- MAR1122, MAR1132  (Since SW 04.2.00)
 + ACA 21-USB       (Since SW 03.0.01)
 + ACA 22-USB       (Since SW 03.0.01)
 + ACA 22A-USB      (Since SW 03.0.01)
 + M-SFP-SX/LC      (Since SW 03.0.01)
 + M-SFP-MX/LC      (Since SW 03.0.01)
 + M-SFP-LX/LC      (Since SW 03.0.01)
 + M-SFP-LX+/LC     (Since SW 03.0.01)
 + M-SFP-LH/LC      (Since SW 03.0.01)
 + M-SFP-LH+/LC     (Since SW 03.0.01)
 + M-SFP-BIDI       (Since SW 03.0.01)
 + M-SFP-TX/RJ45    (Since SW 04.1.00)
 + M-FAST SFP-LH/LC (Since SW 03.0.01)
 + M-FAST SFP-SM/LC (Since SW 03.0.01)
 + M-FAST SFP-SM+/LC(Since SW 03.0.01)
 + M-FAST SFP-MM/LC (Since SW 03.0.01)
 + M-FAST-SFP-TX/RJ45 (Since SW 04.1.00)

3. History of Changes
=====================

Software Release 03.0.01
------------------------
All functions of L2P:
- Autonegotiation or manual port configuration
- Store and Forward Switching, Flow Control
- VLANs (255 out of 4042), Prioritization (8 Levels mapped to 4 Queues), Tagging
- 4 Strict Priority Queues per port
- Independant VLAN learning
- Dynamic Unicast address entries (up to 8000)
- Static Unicast and Multicast address entries (up to 100)
- Dynamic Multicast address entries (up to 512)
- IGMP-Snooping (v1, v2, Querier, Forward All)
- Rapid Spanning Tree
- HIPER-Ring
- Redundant Coupling of HIPER Rings and network segments
- Port Mirroring (1 source port to 1 monitoring port)
- 1(2) signal contact(s) for status monitoring and manual configuration
- Interface-Statistics, RMON (1,2,3,9)
- BOOTP/DHCP with auto configuration
- BOOTP/DHCP relay with option 82
- Fast aging on linkdown
- TFTP (software update, loading and saving the configuration)
- Auto Configuration Adapter ACA 21-USB (software update, loading and saving the 
  configuration)
- SNMP v1, Traps
- SNMP v2c, v3
- V.24 (System Monitor, Command Line Interface)
- Telnet (Command Line Interface)
- Web Based Management
- Diagnosis and self test function on cold start
- Error logging local
- SysLog support
- ACA21-USB password protection
- Address-based port-security
- HiDiscovery
- IEEE 1588 client without hardware support
- Egress broadcast limiter per port
- LLDP
- SFP management
- HTTP Update
- SNTP Client + Server
- Support for traceroute
- VLAN 0 Mode for switching of priority only tagged frames.
- Unknown Multicast Filtering
- Additional ingress and egress paket limiter
- Bridge-MIB: dot1dTpFdbTable supports addresses from all VLANs
- Security Data Sheet from IAONA integrated
- Management Address Conflict Detection
- Automatic configuration undo
- Access to Management from all VLANs
- HTTP config file transfer
- MRP (Media Redundancy Protocol - IEC Ring)
- Loginname and password up to 32 characters
- Transmission of oversize packets up to 1632 bytes (over SNMP activateable)
- Device status indication
- Disable Learning
- Telnet client in webinterface
- Batterybuffered Realtime Clock
- Telnet Client (Outbound Telnet)
- CLI Scripting
- 802.1x security
- GMRP (with Forward All)
- GVRP
- Cable test (copper only)
- Link Aggregation (up to 7 groups with 4 ports for each group), each dynamic(LACP)
  or static
- SNMPv3 Data Encryption
- SSHv1 (DES)
- Configuration as script load/save
- GMRP Forward all
- Dynamic VLAN configuration protocol GVRP
- Customer specific preconfiguration (script)

Software Release 03.1.00
------------------------
Bugfixes (see issuelist)

New functions of L2P:
- Ethernet/IP Protocol
- Merging of HIPER-Ring and MRP webinterface dialog
- Enhancement of portsecurity to 10 addresses per port
- 1588 Clock Optimizations

Software Release 04.0.00
------------------------
Bugfixes (see issuelist)

New functions of L2P:
- IGMP Snooping Enhancements (Automatic query ports, query ports to portmask configurable)
- �Not in sync� display if configuration in Flash/ACA and running config are different
- Configurable priority of the management agent answers
- QoS: TOS/DSCP prioritization
- Shared VLAN Learning
- Config rollback function: Show IP Address of the monitoring station
- Activation of long frames in WEB interface
- Fast ring redundancy < 10 ms for MACH1000 and RSR20/30
- PROFInet IO Protokoll

Software Release 04.1.00
------------------------
Bugfixes (see issuelist)

New functions of L2P:
- Autocrossing configurable
- RAM Test on/off (with new Bootcode)
- IGMP Snooping Improvements
- RSTP actualization to 802.1D-2004
- Activation of long frames (1552/1632) in CLI interface
- MRP with 200ms ring recovery
- HIPER-Ring: Option to reduce maximum ring recovery time.
- DHCP server per port

Software Release 04.2.00
------------------------
Bugfixes (see issuelist)

New functions of L2P:
- LLDP Enhancement to detect some configuration errors
- Support of MACH1000 with 4 gigabit ports, PoE and service port

Software Release 05.0.00
------------------------
Bugfixes (see issuelist)

New functions of L2E:
- Profinet Version 2.2 (PDEV) for all devices
- Profinet: GSDML file generator enhancements
- Profinet: Standalone GSDML generator
- Profinet: Automatic device exchange with Engineering Station
- Profinet: PnP functionality of port modules
- Profinet: Completion of LLDP-Implementation for PNIO-MIB
- Profinet: Transmission of switch data in Profinet I/O data
- New Sub Ring protocol (Multi Ring) for RSR and MACH1000
- Further boot time speed ups (New bootcode required)
- IEEE1588 (PTP) Version 2
- Optimization of Link detection (Hardware dependend)
- RSTP enhancement (Port Auto Edge)
- RSTP and MRP Ring in combination
- No reset configurable in case of error
- Ethernet/IP: Transmission of DIP switch status in I/O data
- MAC based port security: Ranges with a bitmask configurable

New functions of L2P:
- Show CPU / Memory utilization in CLI
- Configure RMON alarms in CLI
- SNMPv3 Authentication over RADIUS

Software Release 06.0.00
------------------------
Bugfixes (see issuelist)

New functions of L2P:
- Port-Mirroring N:1
- Trap for Configuration Saving and Changing
- Address Relearn Detection
- Duplex Mismatch Detection
- SNTP Client and Server separately configurable
- Removed Security Data Sheet (IAONA)
- Configurable CLI banner
- Switch Dump: Download System Information in one ZIP File
- SNMP Logging
- Configure Syslog Settings over WEB
- Profinet: MRP Configurable over Simatic S7
- Profinet: LLDP Extensions
- Automatic Software Update over ACA21
- Multiple Spanning Tree (MSTP)
- RSTP Standard MIB
- Restricted Management Access

Software Release 07.0.00
------------------------
Bugfixes (see issuelist)

New functions of L2P:
- LLDP-MED
- Voice VLAN
- PoE+ Management
- 802.1x Multi Client Authentication
- Guest VLAN / Unauthenticated VLAN for 802.1x
- RADIUS VLAN Assignment
- New configuration check dialog
- Offline configuration
- Port Monitor (Link flap & CRC)
- Advanced Ring Configuration & Diagnostics for MRP
- Port Security with up to 50 MACs per port
- Reload/Reboot with configurable delay
- SFP power also shown in dBm
- Switch to backup SW via Web
- Automatic script load from ACA21 on boot
- Double-VLAN-Tagging / VLAN-Tunneling

Software Release 07.1.00
------------------------
Bugfixes (see issuelist)

New functions of L2P:
- HTTPs Web login
- DHCP Server VLAN Pools

Software Release 07.1.04
------------------------
Bugfixes (see issuelist)
- Discard unknown multicast option for GMRP

Software Release 08.0.00
------------------------
Bugfixes (see issuelist)

- Ethernet/IP: Configurable TTL and EDS AOP support
- Configuration Signature
- DHCP-Relay per port configuration
- Pre-Login-Banner
- DHCP Option 43 support
- DHCP Server configurable ping check.
- Overload detection (as part of Port Monitor)
- Auto Disable
- Port Mirroring Extensions
- MAC notification
- SSH Version 2
- Strong encryption for HTTPS
- Acceptable frame types "admit only VLAN untagged"
- IEC61850 MMS Server

Software Release 08.0.05
------------------------
Bugfixes (see issuelist)

- Removed Rx Power State for SFPs.
- Device Status extended by Sub-Ring monitoring.

Software Release 09.0.00
------------------------
Bugfixes (see issuelist)

New functions of L2E:
- HiDiscovery v2 support
- DHCP Relay per Interface
- Admin State for System Monitor 1
- Configurable CLI banner via CLI
- Configurable SNMP v1/v2 community synchronization
- MAC Address conflict detection
- Profinet GSDML File Version 2.3
- Configuration locking if Profinet AR is active. 

New functions of L2P:
- Configurable number of addresses per interface for Port Security
- Link Speed and Duplex Monitor in port monitor 
- Auto-disable for Link / Duplex Monitor
- Auto-disable for Port Security


Software Release 09.0.15
------------------------
Bugfixes (see issuelist)
